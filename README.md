# sra to metadata excel file

SRA XML query and parser interface for datasets available through ENA.

To use it you need to provide 3 arguments.

For example:

To generate the excel file for these three projects you can use the following argument:

python3 study_creator.py "DRP005906|DRP007222|DRP007099" example@email.nl myexcelfilename.xlsx

To only create an excel file for one project you can use:

python3 study_creator.py "DRP005906" example@email.nl myexcelfilename.xlsx

The first argument after calling the python script is based on the entrez query syntax so you can make more sophisticated queries using the advanced search engine at https://www.ebi.ac.uk/ena/browser/advanced-search.