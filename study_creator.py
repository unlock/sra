import os
import pickle
import re
import xmltodict
import xlsxwriter
from lat_lon_parser import parse
import dateutil.parser

messages = set()


import os
import sys

from Bio import Entrez
from joblib import Parallel, delayed
from joblib.externals.loky.process_executor import TerminatedWorkerError


def processInput(identifier):
    identifier = str(identifier)
    folder = make_folder(identifier)
    path = folder + "/" + identifier + ".xml"

    if os.path.isfile(path):
        return path

    Entrez.email = sys.argv[2]

    handle = Entrez.efetch(db="sra", id=identifier, rettype="xml", retmode="text")

    content = handle.read()

    with open(path, 'w') as f:
        print(content, file=f)

    return path


def make_folder(identifier):
    folder = ""
    # print(identifier)
    for e in identifier:
        if len(folder) % 4 == 0:
            folder = folder + "/"
        folder = folder + e
    return "./sra" + folder


single = 0
paired = 0
error = 0

machines = set()
illumina_machines = set(["Illumina HiSeq 1000", "Illumina HiSeq 1500", "Illumina HiSeq 2000", "Illumina HiSeq 2500",
                         "Illumina HiSeq 3000", "Illumina HiSeq 4000", "Illumina HiSeq x ten", "Illumina MiSeq", 
                         "NextSeq 500", "Illumina NovaSeq 6000", "Illumina Genome Analyzer IIx"])


def machine_filter(doc):
    """
    Removes all docs that are not of illumina hiseq, miseq or nextseq
    :param doc:
    :return:
    """

    if 'EXPERIMENT_PACKAGE' not in doc['EXPERIMENT_PACKAGE_SET']: return False

    key_set = doc['EXPERIMENT_PACKAGE_SET']['EXPERIMENT_PACKAGE']['EXPERIMENT']['PLATFORM'].keys()
    if 'ILLUMINA' not in key_set:
        message = 'Platform is not illumina but it is ' + ' '.join(key_set)
        if message not in messages:
            print(message)
            messages.add(message)
        return False

    if doc['EXPERIMENT_PACKAGE_SET']['EXPERIMENT_PACKAGE']['EXPERIMENT']['PLATFORM']['ILLUMINA'][
        'INSTRUMENT_MODEL'] not in illumina_machines:
        machine = doc['EXPERIMENT_PACKAGE_SET']['EXPERIMENT_PACKAGE']['EXPERIMENT']['PLATFORM']['ILLUMINA'][
            'INSTRUMENT_MODEL']
        machines.add(machine)
        message = "Platform is not of the accepted models " + machine
        if message not in messages:
            print(message)
            messages.add(message)

        return False

    return True


def paired_check(doc):
    """
    Check if it contains paired files
    :param doc:
    :return:
    """
    global single
    global paired
    global error

    try:
        size = int(doc['EXPERIMENT_PACKAGE_SET']['EXPERIMENT_PACKAGE']['RUN_SET']['RUN']['Statistics']['@nreads'])
        if size != 2:
            single += 1
            return False
        paired += 1
        return True
    except KeyError as e:
        error += 1
        return False
    except TypeError as e:
        error += 1
        return False
    except ValueError as e:
        error += 1
        return False


def setup_project(content):
    project = {
        "Project identifier": "UNLOCK",
        "Project title": "UNLOCK Microbial Potential",
        "Project description": "An open infrastructure for exploring new horizons for research on microbial communities."
    }
    return project


def setup_investigation(content):
    investigation = {
        "Investigation identifier": "SRA_Investigation",
        "Investigation title": "Automatically generated SRA investigation",
        "Investigation description": "Automatically generated metadata from SRA information",
        "Project identifier":"UNLOCK",
        "Firstname": "Jasper",
        "Lastname": "Koehorst",
        "Email address": "jasper.koehorst@wur.nl",
        "ORCID": "0000-0001-8172-8981",
        "Organization": "Wageningen University",
        "Department": "UNLOCK",
        "Role": "Administrator"
    }
    return investigation


def setup_study(content):
    study = {"Investigation identifier":"SRA_Investigation"}

    # ORIGINAL RESEARCHER INFORMATION

    # SUBMITTER INFORMATION
    submission = content['SUBMISSION']
    study['submission_lab_name'] = submission['@lab_name']
    if '@center_name' in submission:
        study['submission_center_name'] = submission['@center_name']

    study['submission_accession'] = submission['@accession']
    study['submission_alias'] = submission['@alias']

    # ORGANIZATION INFORMATION
    organization = content['Organization']

    if 'Contact' in organization:
        if type(organization['Contact']) == list:
            print("MULTIPLE CONTACTS DETECTED... -.-")
            print(organization['Contact'])
        study['Email address'] = organization['Contact']['@email']
        # organization['Contact']['@sec_email']

        if 'Name' in organization['Contact']:
            if 'Middle' in organization['Contact']['Name']:
                study['middle_name'] = organization['Contact']['Name']['Middle']
            study['first_name'] = organization['Contact']['Name']['First']
            study['last_name'] = organization['Contact']['Name']['Last']

        if 'Address' in organization['Contact']:
            study['postal_code'] = organization['Contact']['Address']['@postal_code']
            study['department'] = organization['Contact']['Address']['Department']
            study['institution'] = organization['Contact']['Address']['Institution']
            study['street'] = organization['Contact']['Address']['Street']
            study['city'] = organization['Contact']['Address']['City']

            if 'Sub' in organization['Contact']['Address']:
                study['sub'] = organization['Contact']['Address']['Sub']
            if 'Country' in organization['Contact']['Address']:
                study['country'] = organization['Contact']['Address']['Country']

    # STUDY INFORMATION
    study_object = content['STUDY']
    study['Study identifier'] = study_object['@accession']
    study['alias'] = study_object['@alias']
    study['Study title'] = study_object['DESCRIPTOR']['STUDY_TITLE']
    if 'STUDY_ABSTRACT' in study_object['DESCRIPTOR']:
        study['Study description'] = study_object['DESCRIPTOR']['STUDY_ABSTRACT']
    else:
        study['Study description'] = "No study description available for " +study_object['@accession']
    if 'STUDY_TYPE' in study_object['DESCRIPTOR']:
        study['type'] = study_object['DESCRIPTOR']['STUDY_TYPE']['@existing_study_type']

    if 'CENTER_PROJECT_NAME' in study_object['DESCRIPTOR']:
        study['center_project_name'] = study_object['DESCRIPTOR']['CENTER_PROJECT_NAME']

    if 'STUDY_LINKS' in study_object:
        for study_link in study_object['STUDY_LINKS']:
            if 'XREF_LINK' in study_object['STUDY_LINKS'][study_link]:
                xref = study_object['STUDY_LINKS'][study_link]['XREF_LINK']
                study['xref_' + xref['DB']] = xref['ID']
    return study

def setup_observation_unit(content):
    observation_unit = {}

    experiment = content['EXPERIMENT']
    sample = content['SAMPLE']

    observation_unit['Observational unit identifier'] = "X" + sample['@accession']
    observation_unit['alias'] = "X" + sample['@alias']
    observation_unit['Study identifier'] = experiment['STUDY_REF']['@accession']
    
    if "TITLE" in sample:
        observation_unit['Observational unit description'] = sample['TITLE']
        observation_unit['Observational unit name'] = sample['TITLE']
    else:
        observation_unit['Observational unit description'] = 'Automatic OU title from SRA ' + sample['@accession']
        observation_unit['Observational unit name'] = 'Automatic OU title from SRA ' + sample['@accession']

    return observation_unit

def setup_sample(content):
    sample = {}
    sample_object = content['SAMPLE']
    sample['alias'] = sample_object['@alias']
    sample['Sample identifier'] = sample_object['@accession']
    sample['Biosafety level'] = "0"
    sample['collection date'] = "1900-01-01T00:00:00"

    if type(sample_object['IDENTIFIERS']['EXTERNAL_ID']) == list:
        for entry in sample_object['IDENTIFIERS']['EXTERNAL_ID']:
            if entry['@namespace'] == 'BioSample':
                sample_object['IDENTIFIERS']['EXTERNAL_ID'] = entry

    sample['Observational unit identifier'] = "X" + sample_object['@accession']
    sample['namespace'] = sample_object['IDENTIFIERS']['EXTERNAL_ID']['@namespace']
    sample['text'] = sample_object['IDENTIFIERS']['EXTERNAL_ID']['#text']

    # TODO check which name is used in the end
    if "TITLE" in sample_object:
        sample['Sample title'] = sample_object['TITLE']
        sample['sample name'] = sample_object['TITLE']
    else:
        sample['Sample title'] = 'Automatic sample title from SRA ' + sample['Sample identifier']
        sample['sample name'] = 'Automatic sample name from SRA ' + sample['Sample identifier']

    sample['NCBI taxonomy ID'] = sample_object['SAMPLE_NAME']['TAXON_ID']

    # TODO check which name is used in the end
    if 'SCIENTIFIC_NAME' in sample_object['SAMPLE_NAME']:
        sample['Organism name'] = sample_object['SAMPLE_NAME']['SCIENTIFIC_NAME']
        sample['Sample organism'] = sample_object['SAMPLE_NAME']['SCIENTIFIC_NAME']

    if "DESCRIPTION" in sample_object:
        sample['Sample description'] = sample_object['DESCRIPTION']
    else:
        sample['Sample description'] = 'Automatic sample description from SRA ' + sample['Sample identifier']

    sample['attributes'] = {}
    if 'SAMPLE_ATTRIBUTES' in sample_object:
        for sample_attribute in sample_object['SAMPLE_ATTRIBUTES']['SAMPLE_ATTRIBUTE']:
            TAG = sample_attribute['TAG']
            VALUE = sample_attribute['VALUE']
            if TAG == 'sample name':
                TAG = 'TAG sample name'
            # Filtering
            VALUE = filtering(TAG, VALUE)
            sample['attributes'][TAG] = VALUE

    return sample


def setup_assay(content):
    assay = {}
    assay['Facility'] = 'Unknown'
    assay['Method'] = 'Unknown'
    # assay['Date'] = 'Unknown'
    assay['Target subfragment'] = ""
    assay['Isolation protocol'] = "Unknown"

    run = content['RUN_SET']['RUN']
    assay['Assay identifier'] = run['@accession']
    assay['alias'] = run['@alias']
    assay['total_spots'] = run['@total_spots']
    assay['total_bases'] = run['@total_bases']
    assay['size'] = run['@size']
    assay['load_done'] = run['@load_done']
    assay['published'] = run['@published']
    assay['Sample identifier'] = content['SAMPLE']['@accession']
    assay['Study identifier'] = content['STUDY']['@accession']
    assay['Assay name'] = 'Automatic assay title from SRA ' + assay['Assay identifier']
    assay['Assay description'] = 'Automatic assay description from SRA ' + assay['Assay identifier']
    assay['experiment'] = content['EXPERIMENT']['@accession']
    
    # Files
    sra_file = run['SRAFiles']['SRAFile']

    # If it is a list we need the SRR entry
    if type(sra_file) == list:
        status = False
        for sra in sra_file:
            if re.match("SRR[0-9]+", sra['@filename']) or re.match("ERR[0-9]+", sra['@filename']):
                sra_file = sra
                status = True
                break
        if not status:
            for sra in sra_file:
                print(sra['@filename'])
            print("SOMETHING WENT WRONG!")

    assay['Forward filename'] = sra_file['@filename'] + "_1.fastq.gz"
    assay['Reverse filename'] = sra_file['@filename'] + "_2.fastq.gz"
    assay['Date'] = sra_file['@date']
    assay['super_type'] = sra_file['@supertype']
    assay['sratoolkit'] = sra_file['@sratoolkit']

    # Obtain experiment info on design / library
    library = content['EXPERIMENT']['DESIGN']['LIBRARY_DESCRIPTOR']

    if 'LIBRARY_NAME' in library:
        assay['Library name'] = library['LIBRARY_NAME']
    
    assay['Library strategy'] = library['LIBRARY_STRATEGY']
    assay['Library source'] = library['LIBRARY_SOURCE']
    assay['Library selection'] = library['LIBRARY_SELECTION']
    assay['Library layout'] = list(dict(library['LIBRARY_LAYOUT']).keys())

    # Machine
    assay['Instrument model'] = content['EXPERIMENT']['PLATFORM']['ILLUMINA']['INSTRUMENT_MODEL']
    
    if 'ILLUMINA' in content['EXPERIMENT']['PLATFORM']:
        assay['Platform'] = 'Illumina'
    else:
        print("NEW SEQUENCING PLATFORM DETECTED!!!")

    if len(assay['Library layout']) == 1:
        assay['Library layout'] = assay['Library layout'][0]

    assay['Read length'] = run['Statistics']['Read'][0]['@average']

    return assay
    
    # OPTIONAL FILTER FOR LENGTH?... run['Statistics']['READ'][0]['@average']


def create_header(header, keys):
    for value in sorted(keys):
        if value not in header:
            header.append(value)
    return header


def filtering(TAG, VALUE):
    # Fix date representation
    if TAG == 'collection_date':
        try:
            VALUE = str(dateutil.parser.parse(VALUE)).replace(" ", "T")
        except dateutil.parser._parser.ParserError:
            # e.g. 2018/2019
            if re.match("\d\d\d\d/\d\d\d\d", VALUE):
                VALUE = str(dateutil.parser.parse(VALUE.split("/")[0])).replace(" ", "T")
            # e.g. "missing" or empty
            elif re.match("^[a-zA-Z\s-]*$", VALUE):
              VALUE = str(dateutil.parser.parse("01-01-1900")).replace(" ", "T")  
            else:
                print("Failed parsing ", VALUE)
        if "+" in VALUE:
            VALUE = VALUE.split("+00:00")[0]
            
    if TAG in {"env_broad_scale", "env_local_scale","env_medium"}:
        # "match forest biome \[ENVO:[0-9]\]"
        if re.match(".*\[ENVO:[0-9]+\]",""):
            return VALUE
        else:
            VALUE = VALUE.strip() + " [ENVO:00000000]"
    if TAG == "geo_loc_name":
        VALUE = VALUE.replace(":", ";").replace(",", ";")
        if VALUE.count(";") != 2:
            VALUE = VALUE + ";Unknown" * (2 - VALUE.count(";"))
    if TAG == "lat_lon":
        # Check for NESW
        VALUE = VALUE.strip()
        if not re.match("^(-?\d+(\.\d+)?) (-?\d+(\.\d+)?)$", VALUE):
            if re.match(".*[NESW].*", VALUE):
                TEMP = VALUE.split()
                if len(TEMP) == 4:
                    lat = parse(TEMP[0] + TEMP[1])
                    lon = parse(TEMP[2] + TEMP[3])
                    VALUE = str(lat) + " " + str(lon)
            # A string with only letters cannot be parsed
            elif re.match("[a-z]+", VALUE.lower()):
                VALUE = ""
            else:
                print("New parser needed for " + [VALUE])

    return VALUE


def create_xlsx(pickle_list):
    # Prepocessing of picke list
    project_keys = set()
    investigation_keys = set()
    study_keys = set()
    observation_unit_keys = set()
    sample_keys = set()
    assay_keys = set()

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(EXCEL_FILE)
    project_worksheet = workbook.add_worksheet(name="Project")
    investigation_worksheet = workbook.add_worksheet(name="Investigation")
    study_worksheet = workbook.add_worksheet(name="Study")
    ou_worksheet = workbook.add_worksheet(name="ObservationUnit")
    sample_worksheet = workbook.add_worksheet(name="Sample")
    # Todo work on different assay types
    assay_worksheet = workbook.add_worksheet(name="Assay")

    for pickle_file in pickle_list:
        with open(pickle_file, 'rb') as pkl:
            project = pickle.load(pkl)
            project_keys.update(project.keys())
            investigation = pickle.load(pkl)
            investigation_keys.update(investigation.keys())
            study = pickle.load(pkl)
            study_keys.update(study.keys())
            observation_unit = pickle.load(pkl)
            observation_unit_keys.update(observation_unit.keys())
            sample = pickle.load(pkl)
            sample_keys.update(sample.keys())
            sample_keys.update(sample['attributes'].keys())
            sample_keys.remove("attributes")
            assay = pickle.load(pkl)
            assay_keys.update(assay.keys())

    # Creating the headers
    project_header = ["Project identifier", "Project description", "Project title"]
    project_header = create_header(project_header, project_keys)

    investigation_header = ["Investigation identifier", "Investigation description", "Investigation title",
                            "Project identifier", "Firstname", "Lastname", "Email address", "ORCID", "Organization",
                            "Department", "Role"]
    investigation_header = create_header(investigation_header, investigation_keys)

    study_header = ["Study identifier", "Study description", "Study title", "Investigation identifier"]
    study_header = create_header(study_header, study_keys)
    
    observation_unit_header = ["Observational unit identifier", "Observational unit description", "Observational unit name", "Study identifier"]
    observation_unit_header = create_header(observation_unit_header, observation_unit_keys)

    sample_header = ["Sample identifier", "Sample description", "sample name", "Observational unit identifier", "NCBI taxonomy ID", "Sample organism"]
    sample_header = create_header(sample_header, sample_keys)

    assay_header = ["Assay identifier", "Sample identifier", "Assay description", "Forward filename",
                    "Reverse filename", "Forward primer", "Reverse primer", "Primer names", "Isolation protocol",
                    "Sequencing center", "Platform", "Date"]
    assay_header = create_header(assay_header, assay_keys)

    for index, value in enumerate(project_header):
        project_worksheet.write(0, index, value)

    for index, value in enumerate(investigation_header):
        investigation_worksheet.write(0, index, value)

    for index, value in enumerate(study_header):
        study_worksheet.write(0, index, value)

    for index, value in enumerate(observation_unit_header):
        ou_worksheet.write(0, index, value)

    for index, value in enumerate(sample_header):
        sample_worksheet.write(0, index, value)

    for index, value in enumerate(assay_header):
        assay_worksheet.write(0, index, value)

    ###############################################################
    # Filling the project sheet
    ###############################################################
    project_identifier = set()
    row_number = 1
    for row, pickle_file in enumerate(pickle_list):
        with open(pickle_file, 'rb') as pkl:
            project = pickle.load(pkl)
            investigation = pickle.load(pkl)
            study = pickle.load(pkl)
            observation_unit = pickle.load(pkl)
            sample = pickle.load(pkl)
            assay = pickle.load(pkl)

            # Skip studies that are already parsed from other pickles
            if project['Project identifier'] in project_identifier: continue
            for key in project:
                column = project_header.index(key)
                project_worksheet.write(row_number, column, project[key])
                project_identifier.add(project['Project identifier'])
            row_number = row_number + 1

    ###############################################################
    # Filling the investigation sheet
    ###############################################################
    investigation_identifier = set()
    row_number = 1
    for pickle_file in pickle_list:
        with open(pickle_file, 'rb') as pkl:
            project = pickle.load(pkl)
            investigation = pickle.load(pkl)
            study = pickle.load(pkl)
            observation_unit = pickle.load(pkl)
            sample = pickle.load(pkl)
            assay = pickle.load(pkl)

            # Skip studies that are already parsed from other pickles
            if investigation['Investigation identifier'] in investigation_identifier: continue
            for key in investigation:
                column = investigation_header.index(key)
                investigation_worksheet.write(row, column, investigation[key])
                investigation_identifier.add(investigation['Investigation identifier'])
            row_number = row_number + 1

    ###############################################################
    # Filling the study sheet
    ###############################################################
    study_identifier = set()
    row_number = 1
    for pickle_file in pickle_list:
        with open(pickle_file, 'rb') as pkl:
            project = pickle.load(pkl)
            investigation = pickle.load(pkl)
            study = pickle.load(pkl)
            observation_unit = pickle.load(pkl)
            sample = pickle.load(pkl)
            assay = pickle.load(pkl)

            # Skip studies that are already parsed from other assay pickles
            if study['Study identifier'] in study_identifier: continue
            for key in study:
                column = study_header.index(key)
                study_worksheet.write(row_number, column, study[key])
                study_identifier.add(study['Study identifier'])
            row_number = row_number + 1

    ###############################################################
    # Filling the observation unit sheet
    ###############################################################
    ou_identifier = set()
    row_number = 1
    for pickle_file in pickle_list:
        with open(pickle_file, 'rb') as pkl:
            project = pickle.load(pkl)
            investigation = pickle.load(pkl)
            study = pickle.load(pkl)
            observation_unit = pickle.load(pkl)
            sample = pickle.load(pkl)
            assay = pickle.load(pkl)

            # Skip studies that are already parsed from other pickles
            if observation_unit['Observational unit identifier'] in ou_identifier: continue
            for key in observation_unit:
                column = observation_unit_header.index(key)
                ou_worksheet.write(row_number, column, observation_unit[key])
                ou_identifier.add(observation_unit['Observational unit identifier'])
            row_number = row_number + 1
    ###############################################################
    # Filling the sample sheet
    ###############################################################
    sample_identifier = set()
    row_number = 1
    for row, pickle_file in enumerate(pickle_list):
        with open(pickle_file, 'rb') as pkl:
            project = pickle.load(pkl)
            investigation = pickle.load(pkl)
            study = pickle.load(pkl)
            observation_unit = pickle.load(pkl)
            sample = pickle.load(pkl)
            assay = pickle.load(pkl)

            # Skip studies that are already parsed from other pickles
            if sample['Sample identifier'] in sample_identifier: continue
            attributes = sample.pop("attributes")
            sample = {**sample, **attributes}

            for key in sample:
                column = sample_header.index(key)
                sample_worksheet.write(row_number, column, sample[key])
                sample_identifier.add(sample['Sample identifier'])
            row_number = row_number + 1

    ###############################################################
    # Filling the assay sheet
    ###############################################################
    assay_identifier = set()
    row_number = 1
    for pickle_file in pickle_list:
        with open(pickle_file, 'rb') as pkl:
            project = pickle.load(pkl)
            investigation = pickle.load(pkl)
            study = pickle.load(pkl)
            observation_unit = pickle.load(pkl)
            sample = pickle.load(pkl)
            assay = pickle.load(pkl)

            # Skip studies that are already parsed from other pickles
            if assay['Assay identifier'] in assay_identifier: continue
            for key in assay:
                column = assay_header.index(key)
                assay_worksheet.write(row_number, column, assay[key])
                assay_identifier.add(assay['Assay identifier'])
            row_number = row_number + 1

    workbook.close()
    print("done")


def lookup_creation(doc, pickle_file):
    content = doc['EXPERIMENT_PACKAGE_SET']['EXPERIMENT_PACKAGE']
    # if int(content['RUN_SET']['RUN']['@total_spots']) < MINIMUM_NUMBER_OF_SPOTS:
    #     return None

    # if content['RUN_SET']['RUN']['Statistics']['@nreads'] != "2":
    #     return None

    project = setup_project(content)
    investigation = setup_investigation(content)
    study = setup_study(content)
    observation_unit = setup_observation_unit(content)
    sample = setup_sample(content)
    assay = setup_assay(content)

    # Dump into file for merging with other xml files
    pickle_file = open(pickle_file, 'wb')
    pickle.dump(project, pickle_file)
    pickle.dump(investigation, pickle_file)
    pickle.dump(study, pickle_file)
    pickle.dump(observation_unit, pickle_file)
    pickle.dump(sample, pickle_file)
    pickle.dump(assay, pickle_file)
    pickle_file.close()

def selection(content):
    """
    Selection function based on criteria set by the developer
    :param file:
    :return:
    """
    if "" in content:
        # print("Filter disabled")
        return True

    # if "ERP119217" in content:
    #     print("Match detected")
    #     return True

    # if "PRJNA527973" in content or "PRJNA517152" in content:
        # return True

    # if "soil" not in content:
    #     return False

    print("Did not pass the selection filter")
    return False


def main(identifiers):
    pickle_list = set()
    
    for xml in identifiers.values():
        if not xml.endswith(".xml"): continue
        with open(xml) as fd:
            content = fd.read()
            if 'SRAFiles' not in content:
                continue
            if selection(content):
                doc = xmltodict.parse(content)

                if not machine_filter(doc):
                    # print("Not passing machine filter", elem)
                    continue

                if not paired_check(doc):
                    print("Not passing paired check", xml)
                    continue

                # Create lookup file
                pickle_file = xml.replace(".xml", ".pkl")

                if os.path.exists(pickle_file):
                    os.remove(pickle_file)

                if not os.path.exists(pickle_file):
                    print("Creating pickle", pickle_file)
                    lookup_creation(doc, pickle_file)

                pickle_list.add(pickle_file)
    
    # Create EXCEL file
    create_xlsx(pickle_list)


if __name__ == '__main__':
    # An entrez esearch SRA query or a list of projects separated by '|' PRJ00XXXX|PRJ00XXXX
    query = sys.argv[1]
    # Your email adress
    Entrez.email = sys.argv[2]
    handle = Entrez.esearch(db="sra",term=query,retmax="3000000")
    record = Entrez.read(handle)
    
    identifiers = {}
    
    for index, identifier in enumerate(record['IdList']):
        folder = make_folder(identifier)
        path = folder + "/" + identifier + ".xml"
        identifiers[identifier] = path
        if os.path.exists(path):
            continue
        if not os.path.exists(folder):
            os.makedirs(folder)

    num_cores = 2

    print("Parsing", len(identifiers))
    
    # try:
        # results = Parallel(n_jobs=num_cores)(delayed(processInput)(i) for i in identifiers.keys())
    # except Exception as e:
        # pass

    for index, i in enumerate(sorted(set(identifiers.keys()))):
        print("Processing", index,"of",len(identifiers), identifiers[i])
        processInput(i)

    # Excel file
    EXCEL_FILE = sys.argv[3]
    main(identifiers)
